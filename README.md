# README #

This is a small shell script for setting up a base wordpress installation.
It fetches the latest wordpress, downloads the BaseQN theme, cleans up the folder structure and pushes the files to the server.
Also sets up an sftp-config file for FTP based workflow.